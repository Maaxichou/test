<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\BDRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    
    /**
     * @Route("/", name="auth")
     */
    public function new(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, BDRepository $bdrepo)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ROLE_USER']);
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('app_login');
        }
        return $this->render('auth/register.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'AuthController',
            'asterix' => $bdrepo->find(1),
            'tintin' => $bdrepo->find(2)
        ]);
    }
}
