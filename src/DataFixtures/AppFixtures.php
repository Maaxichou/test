<?php

namespace App\DataFixtures;

use App\Entity\BD;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $bd1 = new BD();
        $bd1->setTitle('Astérix chez les Bretons')
            ->setImgPath('https://images-na.ssl-images-amazon.com/images/I/51bnnyoecSL._SX378_BO1,204,203,200_.jpg')
            ->setAuthor('René Goscinny - Albert Uderzo')
            ->setParution(new DateTime('1966-08-31'))
            ->setDescription("Jules César vient d’envahir la Bretagne, mais un village du Cantium (nom d'une civitas devenue le Kent), résiste encore aux légions romaines. Un de ses habitants, le Breton Jolitorax, est dépêché en Gaule pour quérir l’aide des habitants du village de son cousin Astérix, réputé pour sa potion magique qui multiplie la force. Astérix et Obélix le raccompagnent en Bretagne afin de transporter un gros tonneau de potion magique.");
        $manager->persist($bd1);

        $bd2 = new BD();
        $bd2->setTitle("L'Étoile mystérieuse")
            ->setImgPath('https://images-na.ssl-images-amazon.com/images/I/91x9t8kIAJL.jpg')
            ->setAuthor('Hergé')
            ->setParution(new DateTime('1942-05-21'))
            ->setDescription("Jules César vient d’envahir la Bretagne, mais un village du Cantium (nom d'une civitas devenue le Kent), résiste encore aux légions romaines. Un de ses habitants, le Breton Jolitorax, est dépêché en Gaule pour quérir l’aide des habitants du village de son cousin Astérix, réputé pour sa potion magique qui multiplie la force. Astérix et Obélix le raccompagnent en Bretagne afin de transporter un gros tonneau de potion magique.");
        $manager->persist($bd2);
        $manager->flush();
    }
}
